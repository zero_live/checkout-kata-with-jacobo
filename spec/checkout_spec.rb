
describe 'Checkout' do
  it 'starts empty' do
    initial_total = 0
    checkout = Checkout.new

    total = checkout.calculate_total()

    expect(total).to eq(initial_total)
  end

  it 'scans products' do
    product_price = 15
    checkout = Checkout.new

    checkout.scan('D')

    total = checkout.calculate_total()
    expect(total).to eq(product_price)
  end

  it 'scans products' do
    product_price = 20
    checkout = Checkout.new

    checkout.scan('C')

    total = checkout.calculate_total()
    expect(total).to eq(product_price)
  end

  it 'applies discounts' do
    discount_price = 45
    checkout = Checkout.new

    checkout.scan('B')
    checkout.scan('B')

    total = checkout.calculate_total()
    expect(total).to eq(discount_price)
  end

  it 'applies discounts' do
    discount_price = 130
    checkout = Checkout.new

    checkout.scan('A')
    checkout.scan('A')
    checkout.scan('A')

    total = checkout.calculate_total()
    expect(total).to eq(discount_price)
  end

  it 'applies discounts' do
    discount_price = 175
    checkout = Checkout.new

    checkout.scan('A')
    checkout.scan('A')
    checkout.scan('A')
    checkout.scan('B')
    checkout.scan('B')

    total = checkout.calculate_total()
    expect(total).to eq(discount_price)
  end
end

class Checkout
  INITIAL_TOTAL = 0
  CATALOG = {
    'A' => 50,
    'B' => 30,
    'C' => 20,
    'D' => 15
  }

  def initialize
    @list = []
  end

  def calculate_total
    total = INITIAL_TOTAL

    @list.each do |product|
      total += CATALOG[product]
    end

    total -= discounts
    return total
  end

  def scan(product)
    @list.push(product)
  end

  private

  def discounts
    quantity = 0

    quantity += 15 if(@list.count('B') == 2)
    quantity += 20 if(@list.count('A') == 3)

    return quantity
  end
end
